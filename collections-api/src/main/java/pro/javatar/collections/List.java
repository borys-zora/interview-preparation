package pro.javatar.collections;

import java.util.Collection;

/**
 * Created by Borys Zora on 1/29/17.
 */
public interface List<E> {

    /** @see java.util.List#add(Object) */
    boolean add(E e);

    /** @see java.util.List#remove(int) */
    E remove(int index);

    /** @see java.util.List#remove(Object) */
    boolean remove(Object o);

    /** @see java.util.List#get(int) */
    E get(int index);

    /** @see java.util.List#set(int, Object) */
    E set(int index, E element);

    /** @see java.util.List#add(int, Object) */
    void add(int index, E element);

    /** @see java.util.List#size() */
    int size();

    /** @see java.util.List#clear() */
    void clear();

    /** @see java.util.List#isEmpty() */
    boolean isEmpty();

    /** @see java.util.List#contains(Object) */
    boolean contains(Object o);

    /** @see java.util.List#indexOf(Object) */
    int indexOf(Object o);
}
