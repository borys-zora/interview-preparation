package pro.javatar.collections;

/**
 * Created by Borys Zora on 1/29/17.
 */
public interface Stack<E> {

    /**
     * add element to stack
     * @param element
     */
    void push(E element);

    /**
     * remove and return the element
     * @return element most recently pushed
     */
    E pop();

    /**
     * @return boolean is the stack empty ?
     */
    boolean isEmpty();

    /**
     * @return int number of objects on the stack
     */
    int size();

}
