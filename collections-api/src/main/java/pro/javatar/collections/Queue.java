package pro.javatar.collections;

/**
 * Created by Borys Zora on 1/29/17.
 */
public interface Queue<E> {

    /**
     * add item to queue
     * @param element
     */
    void enqueue(E element);

    /**
     * remove and return the element
     * @return E element least recently enqueued
     */
    E dequeue();

    /**
     * @return boolean is the queue empty ?
     */
    boolean isEmpty();

    /**
     * @return int number of objects on the queue
     */
    int size();

}
