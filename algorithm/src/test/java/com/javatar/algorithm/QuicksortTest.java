package com.javatar.algorithm;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by Borys Zora on 1/18/17.
 */
public class QuicksortTest extends SortableCommon {

    public QuicksortTest() {
        super(new Quicksort());
    }

    @Test
    public void sortSuper() throws Exception {
        this.sortNumericOddLength();
    }

    @Test
    public void sort() throws Exception {
        Comparable[] given = new Integer[] {2, 1, 3, 6, 7, 4};
        Comparable[] expected = new Integer[] {1, 2, 3, 4, 6, 7};
        new Quicksort().sort(given, 0, given.length - 1);
        assertThat(given, is(expected));
    }

    @Test
    public void sortBug1() throws Exception {
        Comparable[] given = new Integer[] {11, 10, 5, 12, 15, 2, 1, 6, 3, 7, 14, 9, 13, 4, 8};
        Comparable[] expected = new Integer[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        new Quicksort().sort(given, 0, given.length - 1);
        assertThat(given, is(expected));
    }

    @Test
    public void partition() throws Exception {
        Comparable[] given = new Integer[] {2, 1};
        Comparable[] expected = new Integer[] {1, 2};
        int pivot = new Quicksort().partition(given, 0, 1);
        assertThat(given, is(expected));
    }

    @Test
    public void partitionBug1() throws Exception {
        Comparable[] given = new Integer[] {4, 3, 2, 1, 5, 8, 7, 11, 9, 13, 6, 10, 14, 12, 15};
        Comparable[] expected = new Integer[] {1, 3, 2, 4, 5, 8, 7, 11, 9, 13, 6, 10, 14, 12, 15};
        int pivot = new Quicksort().partition(given, 0, 3);
        assertThat(given, is(expected));
        assertThat(pivot, is(0));
    }

    @Test
    public void partitionVerify1() throws Exception {
        Comparable[] given = new Integer[] {1, 2, 3, 4, 6, 7, 5, 8, 12, 10, 14, 9, 13, 11, 15};
        Comparable[] expected = new Integer[] {1, 2, 3, 4, 5, 7, 6, 8, 12, 10, 14, 9, 13, 11, 15};
        int pivot = new Quicksort().partition(given, 4, 6);
        assertThat(given, is(expected));
        assertThat(pivot, is(4));
    }

    @Test
    public void swapAcs() throws Exception {
        Comparable[] given = new Integer[] {1, 2, 3, 4, 5, 7, 6, 8, 12, 10, 14, 9, 13, 11, 15};
        Comparable[] expected = new Integer[] {1, 2, 3, 4, 5, 6, 7, 8, 12, 10, 14, 9, 13, 11, 15};
        new Quicksort().swapAsc(given, 5, 6);
        assertThat(given, is(expected));
    }

    @Test
    public void partitionVerify2() throws Exception {
        Comparable[] given = new Integer[] {1, 2, 5, 3, 6, 7, 4, 8, 12, 10, 14, 9, 13, 11, 15};
        Comparable[] expected = new Integer[] {1, 2, 3, 4, 6, 7, 5, 8, 12, 10, 14, 9, 13, 11, 15};
        int pivot = new Quicksort().partition(given, 1, 6);
        assertThat(given, is(expected));
        assertThat(pivot, is(3));
    }

}