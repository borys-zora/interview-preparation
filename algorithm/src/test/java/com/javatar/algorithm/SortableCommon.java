package com.javatar.algorithm;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Borys Zora on 1/18/17.
 */
public class SortableCommon {

    final Sortable sortable;

    public SortableCommon(Sortable sortable) {
        this.sortable = sortable;
    }

    @Test
    public void sortAlphabetic() throws Exception {
        Comparable[] given = getStub();
        System.out.println(Arrays.toString(given));
        Comparable[] expected = getExpectedSortedStub();
        sortable.sort(given);
        System.out.println(Arrays.toString(given));
        System.out.println(Arrays.toString(expected));
        assertThat(given, is(expected));
    }

    @Test
    public void sortNumericOddLength() throws Exception {
        Comparable[] given = getNumericStub();
        System.out.println(Arrays.toString(given));
        Comparable[] expected = getExpectedSortedNumericStub();
        sortable.sort(given);
        System.out.println(Arrays.toString(given));
        System.out.println(Arrays.toString(expected));
        assertThat(given, is(expected));
    }

    @Test
    public void less() throws Exception {
        assertTrue("A less than B", sortable.less("A", "B"));
        assertTrue("1 less than 2", sortable.less(1, 2));
        assertFalse("Z less than G", sortable.less("Z", "G"));
        assertFalse("4 less than 3", sortable.less(4, 3));
    }

    @Test
    public void greater() throws Exception {
        assertFalse("A greater than B", sortable.greater("A", "B"));
        assertFalse("1 greater than 2", sortable.greater(1, 2));
        assertTrue("Z greater than G", sortable.greater("Z", "G"));
        assertTrue("4 greater than 3", sortable.greater(4, 3));
    }

    @Test
    public void swap() throws Exception {
        Comparable[] given = new String[] {"G", "L"};
        Comparable[] expected = new String[] {"L", "G"};
        sortable.swap(given, 0, 1);
        assertThat(given, is(expected));
    }

    Comparable[] getStub() {
        return new String[] {"G", "L", "K", "A", "M", "C", "B", "K", "S", "E", "H", "F"};
    }

    Comparable[] getExpectedSortedStub() {
        return new String[] {"A", "B", "C", "E", "F", "G", "H", "K", "K", "L", "M", "S"};
    }

    Comparable[] getNumericStub() {
        return new Integer[] {15, 13, 2, 11, 8, 9, 7, 5, 6, 1, 4, 12, 14, 3, 10};
    }

    Comparable[] getExpectedSortedNumericStub() {
        return new Integer[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    }
}
