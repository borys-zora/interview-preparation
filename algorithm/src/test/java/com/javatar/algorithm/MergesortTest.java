package com.javatar.algorithm;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by Borys Zora on 1/16/17.
 */
public class MergesortTest extends SortableCommon {

    Mergesort mergesort = new Mergesort();

    public MergesortTest() {
        super(new Mergesort());
    }

    @Test
    public void merge() throws Exception {
        String[] given = {"A", "C", "E", "B", "D", "F"};
        String[] expected = {"A", "B", "C", "D", "E", "F"};
        mergesort.merge(given, new Comparable[given.length], 0, 2, 5);
        assertThat(given, is(expected));
    }

}