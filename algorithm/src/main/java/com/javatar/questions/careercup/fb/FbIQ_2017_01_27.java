package com.javatar.questions.careercup.fb;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * ----------------------------------------
 Given many coins of 3 different face values, print the combination sums of the coins up to 1000.
 Must be printed in order.

 eg: coins(10, 15, 55)
 print:
 10
 15
 25
 30
 .
 .
 .
 1000
 *  ----------------------------------------
 * Created by Borys Zora on 1/28/17.
 */
public class FbIQ_2017_01_27 {

    // does not work
    public void printCoinsSumInOrder(int first, int second, int third) {
        int[] orderedDesc = new int[]{first, second, third};
        int max = first > second ? (first > third ? first : third) : (second > third ? second : third);
        int min = first < second ? (first < third ? first : third) : (second < third ? second : third);
        int avg = first != max && first != min ? first : (second != max && second != min ? second : third);
        for (int i = min; i <= 1000; i++) {
            if (i % max % avg % min == 0) System.out.println(i);
        }
    }

    /*
    * @author aonecode.com
    * */
    public void printSums(int c1, int c2, int c3) {
        Set<Integer> sums = new HashSet<>();
        sums.add(0);
        for(int sum = 1; sum <= 1000; sum++) {
            if(sums.contains(sum - c1) || sums.contains(sum - c2) || sums.contains(sum - c3)) {
                System.out.println(sum);
                sums.add(sum);
            }
        }
    }
}
