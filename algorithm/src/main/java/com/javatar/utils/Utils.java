package com.javatar.utils;

import java.math.BigInteger;

/**
 * Created by Borys Zora on 1/29/17.
 */
public class Utils {

    public static void print(String s) {
        System.out.println(s);
    }

    public static String bytes(byte number) {
        return String.format("%04d", new BigInteger(Long.toBinaryString((long) number)));
    }

    public static String bytes(int number) {
        return String.format("%08d", new BigInteger(Long.toBinaryString((long) number)));
    }

    public static String bytes(long number) {
        return String.format("%16d", new BigInteger(Long.toBinaryString(number)));
    }

    /*
        print(Integer.toString(100,8)); // prints 144 --octal representation
        print(Integer.toString(100,2)); // prints 1100100 --binary representation
        print(Integer.toString(100,16)); //prints 64 --Hex representation
    */
}
