package com.javatar.other;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.javatar.utils.Utils.*;

/**
    1010 - 0001 = 1001
    1010 + 0110 = 10000
    1010 << 1 = 10100
    1010 >> 1 = 0101
    1001 & 1100 = 1000
    1100 ^ 1010 = 0110
    1100 | 1010 = 1110
    1001 ^ 1001 = 0000
    0xFF - 1 = FE
    0xAB + 0x11 = BC
 @see 'https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html'
 @see 'http://docs.oracle.com/javase/tutorial/java/nutsandbolts/opsummary.html'
 @see 'https://www.tutorialspoint.com/java/java_bitwise_operators_examples.htm'
 @see 'http://sys.cs.rice.edu/course/comp314/10/p2/javabits.html'
 * Created by Borys Zora on 1/29/17.
 */
public class BitManipulation {

    private static Logger logger = LoggerFactory.getLogger(BitManipulation.class);

    public static void main(String[] args) {
        logger.info("1010 - 0001 = {}", bytes((byte)(0b1010 - 0b0001)));
        logger.info("1010 + 0110 = {}", bytes((byte) (0b1010 + 0b0110)));
        logger.info("1010 << 1 = {}", bytes((byte)(0b1010 << 1)));
        logger.info("1010 >> 1 = {}", bytes((byte)(0b1010 >> 1)));
        logger.info("1001 & 1100 = {}", bytes((byte)(0b1001 & 0b1100)));
        logger.info("1100 ^ 1010 = {}", bytes((byte)(0b1100 ^ 0b1010)));
        logger.info("1100 | 1010 = {}", bytes((byte)(0b1100 | 0b1010)));
        logger.info("1001 ^ 1001 = {}", bytes((byte)(0b1001 ^ 0b1001)));
        logger.info("0xFF - 1 = {}", Integer.toString(0xFF - 1, 16).toUpperCase());
        logger.info("0xAB + 0x11 = {}", Integer.toString(0xAB + 0x11, 16).toUpperCase());
//        printLetters();
    }

    static void printLetters() {
        for(char i = "A".charAt(0), j = "a".charAt(0); i < "Z".charAt(0); i++, j++) {
            logger.info("{} - {}", i, bytes(i));
            logger.info("{} - {}", j, bytes(j));
        }

    }
}

