package com.javatar.algorithm;

import java.util.Arrays;

/**
 * Merge Sort created by John van Neumann in 1959
 * Basic plan.
 * - Divide array into two halves.
 * - Recursively sort each half.
 * - Merge two halves.
 *
 * Time Complexity
 * Ω (n log(n)) - best
 * Θ (n log(n)) - average
 * O (n log(n)) - worst
 *
 * Space Complexity
 * O (n) - worst
 */
public class Mergesort implements Sortable {

    @Override
    public void sort(Comparable[] items) {
        Comparable[] extraArray = new Comparable[items.length]; // 2N space needed
        sort(items, extraArray, 0, items.length - 1);
    }

    void sort(Comparable[] items, Comparable[] extra, int startIdx, int endIdx) {
        if (endIdx <= startIdx) return;
        int midIdx = startIdx + (endIdx - startIdx)/2;
        sort(items, extra, startIdx, midIdx);
        sort(items, extra, midIdx + 1, endIdx);
        merge(items, extra, startIdx, midIdx, endIdx);
        System.out.println(Arrays.toString(items));
    }

    void merge(Comparable[] items, Comparable[] extra, int startIdx, int midIdx, int endIdx) {
        copyItemsToExtraArray(items, extra, startIdx, endIdx);

        for(int i = startIdx, j = midIdx + 1, k = startIdx; k <= endIdx; k++) {
            if (i > midIdx) {
                items[k] = extra[j++];
            } else if (j > endIdx) {
                items[k] = extra[i++];
            } else if (less(extra[i], extra[j])) {
                items[k] = extra[i++];
            } else {
                items[k] = extra[j++];
            }
        }
    }

    void copyItemsToExtraArray(Comparable[] items, Comparable[] extra, int startIdx, int endIdx) {
        for(int i = startIdx; i <= endIdx; i++) {
            extra[i] = items[i];
        }
    }

}
