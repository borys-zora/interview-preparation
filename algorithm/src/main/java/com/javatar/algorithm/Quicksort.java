package com.javatar.algorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * Quick Sort created by Sir Charles Antony Richard Hoare in 1980
 * Basic plan.
 *
 * - Shuffle the array
 *
 * - Pick an element, called a pivot, from the array.
 *
 * - Partition so that, for some j
 *   - entry a[j] is in place
 *   - no large entry to the left of j
 *   - no smaller entry to the right of j
 * (Partitioning: reorder the array so that all elements with values less than the pivot come before the pivot,
 * while all elements with values greater than the pivot come after it (equal values can go either way).
 * After this partitioning, the pivot is in its final position. This is called the partition operation.)
 *
 * - Sort each piece recursively
 *(Recursively apply the above steps to the sub-array of elements with smaller values and separately
 * to the sub-array of elements with greater values.)
 *
 * Time Complexity
 * Ω (n log(n)) - best
 * Θ (n log(n)) - average
 * O (n^2) - worst
 *
 * Space Complexity
 * O (log(n)) - worst
 *
 * @see 'https://en.wikipedia.org/wiki/Quicksort'
 */
public class Quicksort implements Sortable {

    @Override
    public void sort(Comparable[] items) {
        shuffle(items);
        sort(items, 0, items.length - 1);
    }

    void sort(Comparable[] items, int startIdx, int endIdx) {
        if (startIdx >= endIdx) return;
        int pivotIdx = partition(items, startIdx, endIdx);
        sort(items, startIdx, pivotIdx - 1);
        sort(items, pivotIdx + 1, endIdx);
    }

    int partition(Comparable[] items, int startIdx, int endIdx) {
        int i = startIdx;
        int j = endIdx - 1;
        Comparable pivot = items[endIdx];
        do {
            for (;greater(items[j], pivot) && j > 0; j--);
            for (;less(items[i], pivot) && i < endIdx; i++);
            if (i < j) swap(items, i, j);
        } while (i < j);
        swap(items, endIdx, i);
        return i;
    }

}
