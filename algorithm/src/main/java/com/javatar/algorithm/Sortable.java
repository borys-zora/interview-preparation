package com.javatar.algorithm;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Borys Zora on 1/16/17.
 */
public interface Sortable {

    void sort(Comparable[] items);

    default void shuffle(Object[] array) {
        if (array == null) throw new IllegalArgumentException("argument array is null");
        Random random = new Random(System.currentTimeMillis());
        int size = array.length;
        for (int i = 0; i < size; i++) {
            int r = i + random.nextInt(size-i);
            Object temp = array[i];
            array[i] = array[r];
            array[r] = temp;
        }
    }

    default boolean less(Comparable a, Comparable b) {
        return a.compareTo(b) < 0;
    }

    default boolean greater(Comparable a, Comparable b) {
        return a.compareTo(b) > 0;
    }

    default void swap(Comparable[] items, int first, int second) {
        Comparable tmp = items[first];
        items[first] = items[second];
        items[second] = tmp;
    }

    default void swapAsc(Comparable[] items, int startIdx, int endIdx) {
        if (greater(items[startIdx], items[endIdx])) {
            swap(items, startIdx, endIdx);
        }
    }

}
