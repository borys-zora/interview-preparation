package com.javatar.collections;

import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

/**
 * Created by Borys Zora on 1/24/17.
 */
public class HashMapTest {

    Map<String, String> map = new HashMap<>();

    // test specification

    @Test
    public void size() throws Exception {
        assertThat(map.size(), is(0));
        map.put("key", "value");
        assertThat(map.size(), is(1));
        map.put("key", "value");
        assertThat(map.size(), is(1));
        map.put("otherKey", "otherValue");
        assertThat(map.size(), is(2));
        map.clear();
        assertThat(map.size(), is(0));
        map.put("key", "value");
        assertThat(map.size(), is(1));
    }

    @Test
    public void isEmpty() throws Exception {
        assertTrue(map.isEmpty());
        map.put("key", "value");
        assertFalse(map.isEmpty());
        map.put("key", "value");
        assertFalse(map.isEmpty());
        map.put("otherKey", "otherValue");
        assertFalse(map.isEmpty());
        map.clear();
        assertTrue(map.isEmpty());
        map.put("key", "value");
        assertFalse(map.isEmpty());
    }

    @Test
    public void containsKey() throws Exception {
        assertFalse(map.containsKey("key"));
        map.put("key", "value");
        assertTrue(map.containsKey("key"));
        map.put("key", "value");
        assertTrue(map.containsKey("key"));
        map.put("otherKey", "otherValue");
        assertTrue(map.containsKey("key"));
        assertTrue(map.containsKey("otherKey"));
        map.clear();
        assertFalse(map.containsKey("key"));
        assertFalse(map.containsKey("otherKey"));
        map.put("key", "value");
        assertTrue(map.containsKey("key"));
    }

    @Test
    public void containsValue() throws Exception {
        assertFalse(map.containsValue("value"));
        map.put("key", "value");
        assertTrue(map.containsValue("value"));
        map.put("key", "value");
        assertTrue(map.containsValue("value"));
        assertFalse(map.containsValue("otherValue"));
        map.put("otherKey", "otherValue");
        assertTrue(map.containsValue("value"));
        assertTrue(map.containsValue("otherValue"));
        map.clear();
        assertFalse(map.containsValue("value"));
        assertFalse(map.containsValue("otherValue"));
        map.put("key", "value");
        assertTrue(map.containsValue("value"));
    }

    @Test
    public void get() throws Exception {
        assertThat(map.get("key"), nullValue());
        map.put("key", "value");
        assertThat(map.get("key"), is("value"));
        map.put("key", "value");
        assertThat(map.get("key"), is("value"));
        assertThat(map.get("otherKey"), nullValue());
        map.put("otherKey", "otherValue");
        assertThat(map.get("key"), is("value"));
        assertThat(map.get("otherKey"), is("otherValue"));
        map.put("key", "differentValue");
        assertThat(map.get("key"), is("differentValue"));
        map.clear();
        assertThat(map.get("key"), nullValue());
        assertThat(map.get("otherKey"), nullValue());
        map.put("key", "value");
        assertThat(map.get("key"), is("value"));
    }

    @Test
    public void put() throws Exception {
        String previousValue = map.put("key", "value");
        assertThat(previousValue, nullValue());
        previousValue = map.put("key", "value");
        assertThat(previousValue, is("value"));
        map.clear();
        previousValue = map.put("key", "value");
        assertThat(previousValue, nullValue());
    }

    @Test
    public void remove() throws Exception {
        String previousValue = map.remove("key");
        assertThat(previousValue, nullValue());
        map.put("key", "value");
        previousValue = map.remove("key");
        assertThat(previousValue, is("value"));
        previousValue = map.remove("key");
        assertThat(previousValue, nullValue());
    }

    @Test
    public void clear() throws Exception {
        assertThat(map.size(), is(0));
        map.put("key", "value");
        assertThat(map.size(), is(1));
        map.clear();
        assertThat(map.size(), is(0));
    }

    // test helper methods


    @Test
    public void getBucketIndex() throws Exception {
        HashMap<Integer, String> hashMap = new HashMap<>();
        assertThat(hashMap.getBucketIndex(0, 16), is(0));
        assertThat(hashMap.getBucketIndex(1, 16), is(1));
        assertThat(hashMap.getBucketIndex(2, 16), is(2));
        assertThat(hashMap.getBucketIndex(3, 16), is(3));
        assertThat(hashMap.getBucketIndex(4, 16), is(4));
        assertThat(hashMap.getBucketIndex(5, 16), is(5));
        assertThat(hashMap.getBucketIndex(6, 16), is(6));
        assertThat(hashMap.getBucketIndex(7, 16), is(7));
        assertThat(hashMap.getBucketIndex(15, 16), is(15));
        assertThat(hashMap.getBucketIndex(16, 16), is(0));
        assertThat(hashMap.getBucketIndex(17, 16), is(1));
        assertThat(hashMap.getBucketIndex(18, 16), is(2));
    }

    @Test
    public void getBucketIndexBug1() throws Exception {
        HashMap<String, String> hashMap = new HashMap<>();
        assertTrue(hashMap.getBucketIndex("otherKey", 16) > -1);
    }

    @Test
    public void getBucketSize() throws Exception {
        HashMap<Integer, String> hashMap = new HashMap<>();
        assertThat(hashMap.getBucketSize(16), is(32));
        assertThat(hashMap.getBucketSize(32), is(64));
        assertThat(hashMap.getBucketSize(64), is(128));
    }

    @Test
    public void isResizingNeeded() throws Exception {
        HashMap<Integer, String> hashMap = new HashMap<>();
        assertThat(hashMap.isResizingNeeded(9, 16, 0.75), is(false));
        assertThat(hashMap.isResizingNeeded(0, 16, 0.75), is(false));
        assertThat(hashMap.isResizingNeeded(12, 16, 0.75), is(false));
        assertThat(hashMap.isResizingNeeded(12, 16, 0.75), is(false));
        assertThat(hashMap.isResizingNeeded(13, 16, 0.75), is(true));
        assertThat(hashMap.isResizingNeeded(14, 16, 0.75), is(true));
        assertThat(hashMap.isResizingNeeded(20, 24, 0.75), is(true));
    }

    @Test
    public void resizingAfterPut() throws Exception {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < 10_000; i++){
            hashMap.put(i, i);
        }
        assertThat(hashMap.size(), is(10_000));
        for (int i = 0; i < 10_000; i++){
            assertThat(hashMap.get(i), is(i));
        }
    }

    @Test
    public void resizingWithCollisions() throws Exception {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        hashMap.put(0, 0);
        hashMap.put(1, 1);
        hashMap.put(16, 16);
        hashMap.put(17, 17);
        hashMap.put(32, 32);
        hashMap.put(64, 64);
        hashMap.put(128, 128);
        hashMap.put(256, 256);
        for (int i = 0; i < 14; i++){
            hashMap.put(i, i);
        }
        assertThat(hashMap.size(), is(20));
        for (int i = 0; i < 14; i++){
            assertThat(hashMap.get(i), is(i));
        }
        for (int i = 0; i < 100; i++){
            hashMap.put(i, i);
        }
        assertThat(hashMap.size(), is(102));
        for (int i = 0; i < 100; i++){
            assertThat(hashMap.get(i), is(i));
        }
        for (int i = 0; i < 10_000; i++){
            hashMap.put(i, i);
        }
        assertThat(hashMap.size(), is(10_000));
        for (int i = 0; i < 10_000; i++){
            assertThat(hashMap.get(i), is(i));
        }
    }
}