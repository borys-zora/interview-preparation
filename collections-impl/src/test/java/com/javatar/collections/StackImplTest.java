package com.javatar.collections;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by Borys Zora on 1/29/17.
 */
public class StackImplTest {

    StackImpl<String> stack = new StackImpl<>();

    @Test
    public void pushPopSizeEmpty() throws Exception {
        assertThat(stack.size(), is(0));
        assertThat(stack.isEmpty(), is(true));

        stack.push("a");
        assertThat(stack.size(), is(1));
        assertThat(stack.isEmpty(), is(false));

        assertThat(stack.pop(), is("a"));
        assertThat(stack.size(), is(0));
        assertThat(stack.isEmpty(), is(true));

        stack.push("a");
        assertThat(stack.size(), is(1));
        assertThat(stack.isEmpty(), is(false));

        stack.push("b");
        assertThat(stack.size(), is(2));
        assertThat(stack.isEmpty(), is(false));

        stack.push("c");
        assertThat(stack.size(), is(3));
        assertThat(stack.isEmpty(), is(false));

        stack.push("d");
        assertThat(stack.size(), is(4));
        assertThat(stack.isEmpty(), is(false));

        assertThat(stack.pop(), is("d"));
        assertThat(stack.size(), is(3));
        assertThat(stack.isEmpty(), is(false));

        assertThat(stack.pop(), is("c"));
        assertThat(stack.size(), is(2));
        assertThat(stack.isEmpty(), is(false));

        assertThat(stack.pop(), is("b"));
        assertThat(stack.size(), is(1));
        assertThat(stack.isEmpty(), is(false));

        assertThat(stack.pop(), is("a"));
        assertThat(stack.size(), is(0));
        assertThat(stack.isEmpty(), is(true));
    }

    @Test(expected = NullPointerException.class)
    public void pop() throws Exception {
        stack.pop();
    }

}