package com.javatar.collections;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by Borys Zora on 1/29/17.
 */
public class QueueImplTest {

    QueueImpl<String> queue = new QueueImpl<>();

    @Test
    public void queue() throws Exception {
        assertThat(queue.size(), is(0));
        assertThat(queue.isEmpty(), is(true));

        queue.enqueue("a");
        assertThat(queue.size(), is(1));
        assertThat(queue.isEmpty(), is(false));

        assertThat(queue.dequeue(), is("a"));
        assertThat(queue.size(), is(0));
        assertThat(queue.isEmpty(), is(true));

        queue.enqueue("a");
        assertThat(queue.size(), is(1));
        assertThat(queue.isEmpty(), is(false));

        queue.enqueue("b");
        assertThat(queue.size(), is(2));
        assertThat(queue.isEmpty(), is(false));

        queue.enqueue("c");
        assertThat(queue.size(), is(3));
        assertThat(queue.isEmpty(), is(false));

        queue.enqueue("d");
        assertThat(queue.size(), is(4));
        assertThat(queue.isEmpty(), is(false));

        assertThat(queue.dequeue(), is("a"));
        assertThat(queue.size(), is(3));
        assertThat(queue.isEmpty(), is(false));

        assertThat(queue.dequeue(), is("b"));
        assertThat(queue.size(), is(2));
        assertThat(queue.isEmpty(), is(false));

        assertThat(queue.dequeue(), is("c"));
        assertThat(queue.size(), is(1));
        assertThat(queue.isEmpty(), is(false));

        assertThat(queue.dequeue(), is("d"));
        assertThat(queue.size(), is(0));
        assertThat(queue.isEmpty(), is(true));
    }

    @Test(expected = NullPointerException.class)
    public void pop() throws Exception {
        queue.dequeue();
    }

}