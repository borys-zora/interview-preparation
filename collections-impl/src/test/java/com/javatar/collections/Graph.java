package com.javatar.collections;

/**
 * Graph terminology:
 * Path - sequence of vertices connected by edges.
 * Cycle - path whose first and last vertices are the same.
 * Vertex - point
 * Edge - line that is connecting 2 vertices
 * Two vertices are connected if there is path between them.
 *
 * Graph processing problems:
 * Path. Is there a path between s and t?
 * Shortest path. What is the shortest path between s and t?
 * Cycle. Is there a cycle in the graph?
 * Euler tour. Is there a cycle that uses each edge exactly once?
 * Hamilton tour. Is there a cycle that uses each vertex exactly once?
 * Connectivity. Is there a way to connect all vertices?
 * MST. What is the best way to connect all vertices?
 * Biconnectivity. Is there a vertex whose removal disconnects the graph?
 * Planarity. Can you drow the graph in the plane with no crossing edges?
 * Graph isomorphism. Do two adjacency lists represent the same graph?
 *
 * Anomalies: self loop & parallel edges
 *
 * Created by Borys Zora on 1/30/17.
 */
public class Graph {

    int verticesAmount;

    public Graph(){}

    /**
     * create graph with verticesAmount
     * @param verticesAmount int - amount of vertices
     */
    public Graph(int verticesAmount) {
        this.verticesAmount = verticesAmount;
    }

    /**
     * add an edge v-w
    */
    public void addEdge(int v, int w) {

    }

    /**
     * @param v vertex
     * @return all vertices adjacent to v
     */
    public Iterable<Integer> adjacent(int v) {
        return null;
    }

    /**
     * @return int number of vertices
     */
    public int getVerticesAmount() {
        return verticesAmount;
    }

    /**
     * @return int number of edges
     */
    public int getEdgesAmount() {
        return 0;
    }

    /**
     * compute degree of v
     * @param v int vertex index
     * @return int degree (number of adjacent vertices to v)
     */
    public int degree(int v) {
        int degree = 0;
        for (int w: adjacent(v)) degree++;
        return degree;
    }

    /**
     * compute max degree
     * @return int max degree
     */
    public int maxDegree() {
        int max = 0;
        for (int v = 0; v < getVerticesAmount(); v++) {
            int currentDegree = degree(v);
            if (currentDegree > max) {
                max = currentDegree;
            }
        }
        return max;
    }

    /**
     * compute avarage degree
     * @return double average degree
     */
    public double averageDegree() {
        return 2.0 * getEdgesAmount() / getVerticesAmount();
    }

    /**
     * @return number of self loops
     */
    public int numberOfSelfLoops() {
        int count = 0;
        for (int v = 0; v < getVerticesAmount(); v++) {
            for (int w: adjacent(v)) {
                if (v == w) count++;
            }
        }
        return count / 2; // each edge count twice
    }

    @Override
    public String toString() {
        return "Graph{" +
                "verticesAmount=" + verticesAmount +
                '}';
    }
}
