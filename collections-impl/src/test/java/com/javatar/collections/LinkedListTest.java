package com.javatar.collections;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

/**
 * Created by Borys Zora on 1/21/17.
 */
public class LinkedListTest {

    LinkedList<Integer> list = new LinkedList<>();

    @Test
    public void add() throws Exception {
        list.add(7);
        assertThat(list.size(), is(1));
        assertThat(list.get(0), is(7));
        list.add(9);
        assertThat(list.size(), is(2));
        assertThat(list.get(1), is(9));
        assertThat(list.get(0), is(7));
    }

    @Test
    public void addByIndex() throws Exception {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertThat(list.size(), is(6));
        list.add(3, 9);
        assertThat(list.size(), is(7));
        // verify elements
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
        assertThat(list.get(2), is(2));
        assertThat(list.get(3), is(9));
        assertThat(list.get(4), is(3));
        assertThat(list.get(5), is(4));
        assertThat(list.get(6), is(5));
    }

    @Test
    public void removeByIndex() throws Exception {
        list.add(0);
        list.add(1);
        list.add(8);
        list.add(3);
        assertThat(list.size(), is(4));
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
        assertThat(list.get(2), is(8));
        assertThat(list.get(3), is(3));
        Integer removedElement = list.remove(2);
        assertThat(removedElement, is(8));
        assertThat(list.size(), is(3));
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
        assertThat(list.get(2), is(3));
        Integer removedLastIndex = list.remove(list.size() - 1);
        assertThat(removedLastIndex, is(3));
        assertThat(list.size(), is(2));
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
    }

    @Test
    public void removeByObject() throws Exception {
        list.add(0);
        list.add(1);
        list.add(8);
        list.add(3);
        assertThat(list.size(), is(4));
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
        assertThat(list.get(2), is(8));
        assertThat(list.get(3), is(3));
        boolean isRemoved = list.remove(Integer.valueOf(8));
        assertTrue(isRemoved);
        assertThat(list.size(), is(3));
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
        assertThat(list.get(2), is(3));
    }

    @Test
    public void get() throws Exception {
        assertThat(list.get(0), nullValue());
        list.add(9);
        assertThat(list.get(0), is(9));
    }

    @Test
    public void set() throws Exception {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertThat(list.size(), is(6));
        Integer previousValue = list.set(3, 9);
        assertThat(previousValue, is(3));
        assertThat(list.size(), is(6));
        // verify elements
        assertThat(list.get(0), is(0));
        assertThat(list.get(1), is(1));
        assertThat(list.get(2), is(2));
        assertThat(list.get(3), is(9));
        assertThat(list.get(4), is(4));
        assertThat(list.get(5), is(5));
    }

    @Test
    public void size() throws Exception {
        assertThat(list.size(), is(0));
        list.add(3);
        assertThat(list.size(), is(1));
        list.add(9);
        assertThat(list.size(), is(2));
        list.remove(0);
        assertThat(list.size(), is(1));
        list.remove(0);
        assertThat(list.size(), is(0));
    }

    @Test
    public void clear() throws Exception {
        assertThat(list.size(), is(0));
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        assertThat(list.size(), is(4));
        list.clear();
        assertThat(list.size(), is(0));
    }

    @Test
    public void isEmpty() throws Exception {
        assertTrue(list.isEmpty());
        list.add(0);
        assertFalse(list.isEmpty());
        list.remove(0);
        assertTrue(list.isEmpty());
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        assertFalse(list.isEmpty());
        list.clear();
        assertTrue(list.isEmpty());
    }

    @Test
    public void contains() throws Exception {
        assertFalse(list.contains(Integer.valueOf(0)));
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        assertTrue(list.contains(Integer.valueOf(0)));
        assertTrue(list.contains(Integer.valueOf(1)));
        assertTrue(list.contains(Integer.valueOf(2)));
        assertTrue(list.contains(Integer.valueOf(3)));
        assertFalse(list.contains(Integer.valueOf(4)));
    }

    @Test
    public void indexOf() throws Exception {
        list.add(0);
        list.add(1);
        list.add(7);
        list.add(3);
        list.add(7);
        list.add(6);
        int index = list.indexOf(Integer.valueOf(6));
        assertThat(index, is(5));
        int indexWithDuplicates = list.indexOf(Integer.valueOf(7));
        assertThat(indexWithDuplicates, is(2));
        int noItemIdx = list.indexOf(Integer.valueOf(4));
        assertThat(noItemIdx, is(-1));
    }
}