package com.javatar.collections;

import pro.javatar.collections.List;

/**
 * Created by Borys Zora on 2/5/17.
 */
public class ArrayList<E> implements List<E> {

    private final static int DEFAULT_ARRAY_LIST_CAPACITY = 16;

    private Object[] elements;

    private int size = 0;

    public ArrayList() {
        elements = new Object[DEFAULT_ARRAY_LIST_CAPACITY];
    }

    public ArrayList(int capacity) {
        elements = new Object[capacity];
    }

    @Override
    public boolean add(E e) {
        if (isResizingNeeded()) resize();
        elements[size++] = e;
        return true;
    }

    @Override
    public E remove(int index) {
        if (index > size) throw new IndexOutOfBoundsException();
        E element = (E) elements[index];
        size--;
        for (int i = index; i < size; i++) {
            elements[i] = elements[i + 1];
        }
        return element;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        remove(index);
        return true;
    }

    @Override
    public E get(int index) {
        return (E) elements[index];
    }

    @Override
    public E set(int index, E element) {
        if (index >= size) throw new IndexOutOfBoundsException();
        E result = (E) elements[index];
        elements[index] = element;
        return result;
    }

    @Override
    public void add(int index, E element) {
        if (isResizingNeeded()) resize();
        if (index >= size) throw new IndexOutOfBoundsException();
        for (int i = size; i >= index; i--) {
            elements[i] = elements[i-1];
        }
        elements[index] = element;
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        size = 0;
        elements = new Object[DEFAULT_ARRAY_LIST_CAPACITY];
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) > -1;
    }

    @Override
    public int indexOf(Object o) {
        if (o == null ) return -1;
        for (int i = 0; i < size; i++ ) {
            if (o.equals(elements[i])) return i;
        }
        return -1;
    }

    // helper methods

    boolean isResizingNeeded() {
        if (size >= elements.length) return true;
        return false;
    }

    void resize() {
        int oldSize = elements.length;
        int newSize = oldSize + (oldSize >> 1);
        Object[] newElements = new Object[newSize];
        for (int i = 0; i < oldSize; i++) {
            newElements[i] = elements[i];
        }
        elements = newElements;
    }
}
