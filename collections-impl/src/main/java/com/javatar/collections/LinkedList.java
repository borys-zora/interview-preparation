package com.javatar.collections;

import pro.javatar.collections.List;

/**
 * Created by Borys Zora on 1/21/17.
 */
public class LinkedList<E> implements List<E> {

    private int size = 0;
    private Item<E> first;
    private Item<E> last;

    public LinkedList() {}

    private static class Item<E> {
        E value;
        Item<E> next;
        Item<E> previous;

        Item(E e) {
            this.value = e;
        }
    }

    // classic methods

    @Override
    public boolean add(E e) {
        Item<E> item = new Item<>(e);
        if(size == 0) {
            first = item;
            last = item;
            first.next = last;
            last.previous = first;
        } else {
            last.next = item;
            Item<E> previousLast = last;
            last = item;
            last.previous = previousLast;
        }
        size++;
        return true;
    }

    @Override
    public E remove(int index) {
        Item<E> result = getItem(index);
        return removeItem(result);
    }

    @Override
    public boolean remove(Object o) {
        if (o == null || isEmpty()) return false;
        Item<E> item = getItem(o);
        return removeItem(item) != null;
    }

    @Override
    public E get(int index) {
        Item<E> result = getItem(index);
        return result == null ? null : result.value;
    }

    @Override
    public E set(int index, E element) {
        Item<E> item = getItem(index);
        E result = item.value;
        item.value = element;
        return result;
    }

    @Override
    public void add(int index, E element) {
        Item<E> item = getItem(index);
        if(item == null) throw new IndexOutOfBoundsException("current LinkedList size is " + size);
        Item<E> newItem = new Item<>(element);
        item.previous.next = newItem;
        newItem.previous = item.previous;
        item.previous = newItem;
        newItem.next = item;
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) return -1;
        Item<E> current = first;
        for(int i = 0; i < size; i++) {
            if (o.equals(current.value)) return i;
            current = current.next;
        }
        return -1;
    }

    // helper methods

    Item<E> getItem(int index) {
        if (index >= size) return null; // throw new IndexOutOfBoundsException("current LinkedList size is " + size);
        Item<E> result = first;
        for(int i = 1; i <= index; i++) {
            result = result.next;
        }
        return result;
    }

    Item<E> getItem(Object o) {
        Item<E> current = first;
        for (int i = 0; i < size; i++) {
            if (o.equals(current.value)) return current;
            current = current.next;
        }
        return null;
    }

    E removeItem(Item<E> item) {
        if (item == null) return null;
        E value = item.value;
        Item<E> next = item.next;
        Item<E> previous = item.previous;
        if (next != null) {
            previous.next = next;
            next.previous = previous;
        } else {
            previous.next = null;
            last = previous;
        }
        size--;
        return value;
    }

}
