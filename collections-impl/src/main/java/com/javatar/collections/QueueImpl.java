package com.javatar.collections;

import pro.javatar.collections.Queue;

/**
 * Created by Borys Zora on 1/29/17.
 */
public class QueueImpl<E> implements Queue<E> {

    private Node first;
    private Node last;
    private int size;

    @Override
    public void enqueue(E element) {
        Node newFirst = new Node(element);
        if (first == null) {
            last = newFirst;
        } else {
            first.previous = newFirst;
        }
        first = newFirst;
        size++;
    }

    @Override
    public E dequeue() {
        E element = last.element;
        last = last.previous;
        if (last == null) first = null;
        size--;
        return element;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    private class Node {
        E element;
        Node previous;

        Node(E element) {
            this.element = element;
        }
    }

}
