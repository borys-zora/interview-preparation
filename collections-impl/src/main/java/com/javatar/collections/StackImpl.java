package com.javatar.collections;

import pro.javatar.collections.Stack;

/**
 * Created by Borys Zora on 1/29/17.
 */
public class StackImpl<E> implements Stack<E> {

    private Node head = null;
    private int size;

    @Override
    public void push(E element) {
        Node newNode = new Node(element);
        if (head != null) newNode.previous = head;
        head = newNode;
        size++;
    }

    @Override
    public E pop() {
        E element = head.element;
        head = head.previous;
        size--;
        return element;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    private class Node {
        E element;
        Node previous;

        Node(E element) {
            this.element = element;
        }
    }

}
