package com.javatar.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by Borys Zora on 1/23/17.
 */
public class HashMap<K, V> implements Map<K, V> {

    private static final double DEFAULT_LOAD_FACTOR = 0.75;
    private static final int DEFAULT_BUCKETS_SIZE = 16;

    private double loadFactor;
    private int currentLoad = 0;
    private int size = 0;
    private MapEntry[] buckets;

    public HashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        this.buckets = new MapEntry[DEFAULT_BUCKETS_SIZE];
    }

    public HashMap(double loadFactor) {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        this.loadFactor = loadFactor;
    }

    public HashMap(int initialCapacity, double loadFactor) {
        this.buckets = new MapEntry[initialCapacity];
        this.loadFactor = loadFactor;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key == null) return false;
        int bucketIdx = getBucketIndex(key, buckets.length);
        MapEntry<K, V> entry = buckets[bucketIdx];
        while (entry != null) {
            if (key.equals(entry.key)) return true;
            entry = entry.next;
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        if (value == null) return false;
        for (int i = 0; i < buckets.length; i++) {
            MapEntry<K, V> entry = buckets[i];
            while (entry != null) {
                if (value.equals(entry.value)) return true;
                entry = entry.next;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        if (key == null) return null;
        int bucketIdx = getBucketIndex(key, buckets.length);
        MapEntry<K, V> entry = buckets[bucketIdx];
        while (entry != null) {
            if (key.equals(entry.key)) return entry.value;
            entry = entry.next;
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        validate(key, value);
        int bucketIdx = getBucketIndex(key, buckets.length);
        if (buckets[bucketIdx] == null) {
            buckets[bucketIdx] = new MapEntry<>(key, value);
            size++;
            currentLoad++;
            if (isResizingNeeded(currentLoad, buckets.length, loadFactor)) {
                buckets = resize(buckets);
            }
            return null;
        }
        MapEntry<K, V> entry = buckets[bucketIdx];
        MapEntry<K, V> previousEntry = null;
        while (entry != null) {
            if (key.equals(entry.key)) {
                V previousValue = entry.value;
                entry.setValue(value);
                return previousValue;
            }
            previousEntry = entry;
            entry = entry.next;
        }
        entry = new MapEntry<>(key, value);
        previousEntry.setNext(entry);
        size++;
        return null;
    }

    @Override
    public V remove(Object key) {
        if (key == null) return null;
        int bucketIdx = getBucketIndex(key, buckets.length);
        MapEntry<K, V> entry = buckets[bucketIdx];
        MapEntry<K, V> previous = null;
        while (entry != null) {
            if (key.equals(entry.key)) {
                V value = entry.value;
                size--;
                if (entry.next == null && previous == null) {
                    buckets[bucketIdx] = null;
                    currentLoad--;
                } else if (entry.next != null && previous == null) {
                    buckets[bucketIdx] = entry.next;
                } else if (entry.next != null && previous != null) {
                    previous.next = entry.next;
                }
                return value;
            }
            previous = entry;
            entry = entry.next;
        }
        return null;
    }

    @Override
    public void clear() {
        buckets = new MapEntry[16];
        currentLoad = 0;
        size = 0;
    }

    // helper methods

    int getBucketIndex(Object key, int bucketsAmount) {
        return Math.abs(key.hashCode() % bucketsAmount);
    }

    void validate(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException("key: " + key + " and value: " + value + " must not bee null");
        }
    }

    int getBucketSize(int currentBucketSize) {
        return (currentBucketSize << 1);
    }

    boolean isResizingNeeded(int currentLoad, int bucketsAmount, double loadFactor) {
        double load = currentLoad * 1.0 / bucketsAmount;
        return Double.compare(load, loadFactor) > 0;
    }

    MapEntry[] resize(MapEntry[] oldBuckets) {
        currentLoad = 0;
        int newBucketsSize = getBucketSize(oldBuckets.length);
        MapEntry[] newBuckets  = new MapEntry[newBucketsSize];
        for (int i = 0; i < oldBuckets.length; i++) {
            MapEntry<K, V> entry = oldBuckets[i];
            while (entry != null) {
                MapEntry<K, V> newEntry = new MapEntry<>(entry.key, entry.value);
                putOnResize(newBuckets, newEntry);
                entry = entry.next;
            }
        }
        return newBuckets;
    }

    void putOnResize(MapEntry[] newBuckets, MapEntry newEntry) {
        int bucketIdx = getBucketIndex(newEntry.key, newBuckets.length);
        if (newBuckets[bucketIdx] == null) {
            newBuckets[bucketIdx] = newEntry;
            currentLoad++;
            return;
        }
        MapEntry<K, V> entry = newBuckets[bucketIdx];
        MapEntry<K, V> previousEntry = null;
        while (entry != null) {
            previousEntry = entry;
            entry = entry.next;
        }
        previousEntry.setNext(newEntry);
    }

    class MapEntry<K, V> implements Entry<K, V> {
        final K key;
        V value;
        MapEntry<K, V> next = null;

        MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V previous = this.value;
            this.value = value;
            return previous;
        }

        public void setNext(MapEntry<K, V> next) {
            this.next = next;
        }
    }

    // other methods

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}
